(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)
(setq make-backup-files nil)
(setq ring-bell-function 'ignore)
(set-default-font "Fira Code Retina 9")

(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))

(setq package-list '(use-package))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

(require 'use-package)

(use-package ivy
  :ensure t
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t)
  )

(use-package swiper
  :ensure t
  :config
  (global-set-key "\C-s" 'swiper))

(use-package counsel
  :ensure t
  :config
  (global-set-key (kbd "M-x") 'counsel-M-x)
  (global-set-key (kbd "C-x C-f") 'counsel-find-file)
  (global-set-key (kbd "<f1> f") 'counsel-describe-function)
  (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
  (global-set-key (kbd "<f1> l") 'counsel-find-library)
  (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
  (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
  (global-set-key (kbd "C-c g") 'counsel-git)
  (global-set-key (kbd "C-c j") 'counsel-git-grep)
  (global-set-key (kbd "C-c a") 'counsel-ag)
  (global-set-key (kbd "C-x l") 'counsel-locate)
  (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history))

(use-package eyebrowse
  :ensure t
  :defer t
  :config
  (eyebrowse-mode t)
  )

(use-package magit
  :ensure t
  :defer t
  )

(use-package forge
  :ensure t
  :after magit)

(use-package ispell
  :ensure t
  :defer t)

(use-package evil
  :ensure t
  :config
  (evil-mode 1)
  )

(use-package rust-mode
  :ensure t
  )

(use-package go-mode
  :ensure t
  )

(use-package flycheck
  :ensure t
  )

(use-package company
  :ensure t
  )

(use-package lsp-mode
  :ensure t
  :hook
  (c++-mode . lsp)
  (rust-mode . lsp)
  :commands lsp
  )

(use-package lsp-ui
  :ensure t
  :defer t
  :hook
  (lsp-mode-hook . lsp-ui-mode)
  :commands lsp-ui-mode
  )

(use-package company-lsp
  :ensure t
  :commands company-lsp
  :config (push 'company-lsp company-backends)
  :defer t
  )

(use-package dap-mode
  :ensure t
  :defer t
  )

(use-package spaceline
  :defer t
  )

(use-package spaceline-config
  :ensure spaceline
  :config
  (setq powerline-height 64)
  (setq powerline-default-separator 'slant)
  (spaceline-helm-mode 1)
  (spaceline-emacs-theme)
  )


(use-package doom-themes
  :ensure t
  )
(use-package blackboard-theme
  :ensure t
  )
(use-package gruvbox-theme
  :ensure t
  )
;;(load-theme 'gruvbox-light-medium t)
(load-theme 'leuven t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("f951343d4bbe5a90dba0f058de8317ca58a6822faa65d8463b0e751a07ec887c" "b462d00de785490a0b6861807a360f5c1e05b48a159a99786145de7e3cce3afe" "f2b83b9388b1a57f6286153130ee704243870d40ae9ec931d0a1798a5a916e76" "2d1fe7c9007a5b76cea4395b0fc664d0c1cfd34bb4f1860300347cdad67fb2f9" "1728dfd9560bff76a7dc6c3f61e9f4d3e6ef9d017a83a841c117bd9bebe18613" "7ffb0d3d0c797b980ed7330adc04a66516d49a61e4187a7054dda014676421d9" "f30aded97e67a487d30f38a1ac48eddb49fdb06ac01ebeaff39439997cbdd869" "a2286409934b11f2f3b7d89b1eaebb965fd63bc1e0be1c159c02e396afb893c8" "43b219a31db8fddfdc8fdbfdbd97e3d64c09c1c9fdd5dff83f3ffc2ddb8f0ba0" "2b9dc43b786e36f68a9fd4b36dd050509a0e32fe3b0a803310661edb7402b8b6" default)))
 '(package-selected-packages
   (quote
    (company lsp-mode helm use-package gruvbox-theme forge eyebrowse evil))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
