return require('packer').startup(function()
    use 'folke/tokyonight.nvim'
    use 'NLKNguyen/papercolor-theme'

    use {
        'hoob3rt/lualine.nvim',
        requires = {'kyazdani42/nvim-web-devicons', opt = true}
    }

    use {
        'nvim-telescope/telescope.nvim',
        requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
    }

    use 'neovim/nvim-lspconfig'
    use 'nvim-lua/completion-nvim'

    use 'SirVer/ultisnips'

    use 'nvim-treesitter/nvim-treesitter'
end)
