" Basics {
	set nocompatible
	set background=dark
	syntax on
	" change map leader
	let mapleader = ","
	let g:mapleader = ","
	set nobackup
	filetype plugin on
	filetype indent on
	set history=700
	set nobackup
	set mouse=a
	set tabpagemax=128
" }

" Vim UI {
	set so=7
	set cursorline
	set incsearch
	set laststatus=2
	set linespace=0
	set matchtime=5
	set novisualbell
	set vb
	set scrolloff=10
	set showcmd
	set showmatch
	set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][%04l,%04v]	
	set guioptions-=m
	set guioptions-=T
	set guioptions-=L
	set guioptions-=r
	set guioptions-=b
	"set list listchars=tab:»·,trail:·
	set wildmenu
	set wildignore=*.o,*~,*.pyc
	set wildmode=list:longest
	set ruler
	set cmdheight=2
	set hid
	set backspace=indent,eol,start
	set whichwrap+=<,>,h,l	
	set ignorecase
	nnoremap / /\v
	vnoremap / /\v
	set smartcase
	set infercase
	set hlsearch
	set gdefault
	nnoremap <leader><space> :noh<cr>
	nnoremap <tab> %
	vnoremap <tab> %
	set magic
	set noerrorbells
	set novisualbell
	set t_vb=
	set tm=500
	set nobackup
	set nowb
	set noswapfile
" }

" Text Formatting/Layout {
	set shiftwidth=8
	set tabstop=8

	set t_Co=256
	colorscheme Tomorrow

	if has("gui_running")
		set guioptions-=T
		set guioptions+=e
		set guitablabel=%M\ %t
		colorscheme twilight
	endif

	set encoding=utf8
	set ffs=unix,dos,mac
" }

" Folding {
	set foldenable
	set foldmarker={,}
	set foldmethod=marker
	set foldlevel=100
	set foldopen=block,hor,mark,percent,quickfix,tag
" }
"
" Configuration for C-like languages.
function! SetupForCLang()
    " Highlight lines longer than 80 characters.
    au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
    " Alternately, uncomment these lines to wrap at 80 characters.
    " setlocal textwidth=80
    " setlocal wrap

    " Use 2 spaces for indentation.
    setlocal shiftwidth=2
    setlocal tabstop=2
    setlocal softtabstop=2
    setlocal expandtab

    " Configure auto-indentation formatting.
    setlocal cindent
    setlocal cinoptions=h1,l1,g1,t0,i4,+4,(0,w1,W4
    setlocal indentexpr=GoogleCppIndent()
    let b:undo_indent = "setl sw< ts< sts< et< tw< wrap< cin< cino< inde<"

    " Uncomment these lines to map F5 to the CEF style checker. Change the path to match your system.
    " map! <F5> <Esc>:!python ~/code/chromium/src/cef/tools/check_style.py %:p 2> lint.out<CR>:cfile lint.out<CR>:silent !rm lint.out<CR>:redraw!<CR>:cc<CR>
    " map  <F5> <Esc>:!python ~/code/chromium/src/cef/tools/check_style.py %:p 2> lint.out<CR>:cfile lint.out<CR>:silent !rm lint.out<CR>:redraw!<CR>:cc<CR>
endfunction

function! GoogleCppIndent()
    let l:cline_num = line('.')

    let l:orig_indent = cindent(l:cline_num)

    if l:orig_indent == 0 | return 0 | endif

    let l:pline_num = prevnonblank(l:cline_num - 1)
    let l:pline = getline(l:pline_num)
    if l:pline =~# '^\s*template' | return l:pline_indent | endif

    " TODO: I don't know to correct it:
    " namespace test {
    " void
    " ....<-- invalid cindent pos
    "
    " void test() {
    " }
    "
    " void
    " <-- cindent pos
    if l:orig_indent != &shiftwidth | return l:orig_indent | endif

    let l:in_comment = 0
    let l:pline_num = prevnonblank(l:cline_num - 1)
    while l:pline_num > -1
        let l:pline = getline(l:pline_num)
        let l:pline_indent = indent(l:pline_num)

        if l:in_comment == 0 && l:pline =~ '^.\{-}\(/\*.\{-}\)\@<!\*/'
            let l:in_comment = 1
        elseif l:in_comment == 1
            if l:pline =~ '/\*\(.\{-}\*/\)\@!'
                let l:in_comment = 0
            endif
        elseif l:pline_indent == 0
            if l:pline !~# '\(#define\)\|\(^\s*//\)\|\(^\s*{\)'
                if l:pline =~# '^\s*namespace.*'
                    return 0
                else
                    return l:orig_indent
                endif
            elseif l:pline =~# '\\$'
                return l:orig_indent
            endif
        else
            return l:orig_indent
        endif

        let l:pline_num = prevnonblank(l:pline_num - 1)
    endwhile

    return l:orig_indent
endfunction

" Formatting {
	set nowrap
	set autoindent
	set shiftwidth=8
	set tabstop=8
	set softtabstop=8
" }


" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

Plug 'vim-scripts/CSApprox'
Plug 'vim-scripts/guicolorscheme'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'Lokaltog/vim-easymotion'
Plug 'flazz/vim-colorschemes'
Plug 'wting/rust.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'junegunn/vim-easy-align'
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'fatih/vim-go'
Plug 'nsf/gocode'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()

" fzf {
	" This is the default extra key bindings
	let g:fzf_action = {
	  \ 'ctrl-t': 'tab split',
	  \ 'ctrl-x': 'split',
	  \ 'ctrl-v': 'vsplit' }

	" Default fzf layout
	" - down / up / left / right
	let g:fzf_layout = { 'down': '~40%' }

	" Customize fzf colors to match your color scheme
	let g:fzf_colors =
	\ { 'fg':      ['fg', 'Normal'],
	  \ 'bg':      ['bg', 'Normal'],
	  \ 'hl':      ['fg', 'Comment'],
	  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
	  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
	  \ 'hl+':     ['fg', 'Statement'],
	  \ 'info':    ['fg', 'PreProc'],
	  \ 'border':  ['fg', 'Ignore'],
	  \ 'prompt':  ['fg', 'Conditional'],
	  \ 'pointer': ['fg', 'Exception'],
	  \ 'marker':  ['fg', 'Keyword'],
	  \ 'spinner': ['fg', 'Label'],
	  \ 'header':  ['fg', 'Comment'] }

	" Enable per-command history.
	" CTRL-N and CTRL-P will be automatically bound to next-history and
	" previous-history instead of down and up. If you don't like the change,
	" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
	let g:fzf_history_dir = '~/.local/share/fzf-history'

	" Mapping selecting mappings
	nmap <leader><tab> <plug>(fzf-maps-n)
	xmap <leader><tab> <plug>(fzf-maps-x)
	omap <leader><tab> <plug>(fzf-maps-o)

	nmap ; :Buffers<CR>
	nmap <Leader>t :Files<CR>
	nmap <Leader>r :Tags<CR>

	" Insert mode completion
	imap <c-x><c-k> <plug>(fzf-complete-word)
	imap <c-x><c-f> <plug>(fzf-complete-path)
	imap <c-x><c-j> <plug>(fzf-complete-file-ag)
	imap <c-x><c-l> <plug>(fzf-complete-line)

	" Advanced customization using autoload functions
	inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})
" }

" Airline {
	let g:airline_theme='tomorrow'
	let g:airline#extensions#tabline#left_sep = ' '
	let g:airline#extensions#tabline#left_alt_sep = '|'
" }

" Easymotion {
	let g:EasyMotion_leader_key = '<Leader>'
" }

" TagBar {
	" map <leader>tt :TagbarToggle<CR>
" }

"" Cscope {
if has("cscope")
	" Look for a 'cscope.out' file starting from the current directory,
	" going up to the root directory.

	if (filereadable("cscope.out"))
		execute "cs add cscope.out"
	else
		let s:dirs = split(getcwd(), "/")
		while s:dirs != []
			let s:path = "/" . join(s:dirs, "/")
			if (filereadable(s:path . "/cscope.out"))
				execute "cs add " . s:path . "/cscope.out " . s:path
				break
			endif
			let s:dirs = s:dirs[:-2]
		endwhile
	endif

	set csto=0
	set cst
	set csverb

	nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
	nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
	nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
	nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
	nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
	nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
	nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
	nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>

	map <F10> :copen<CR>
	map <F11> :cprevious<CR>
	map <F12> :cnext<CR>

	" Open a quicfix window for the following queries.
	set cscopequickfix=s-,c-,d-,i-,t-,e-,g-
endif

" }

" Mappings {
	nnoremap <F5> :GundoToggle<CR>
	nnoremap <F6> :NERDTreeToggle<CR>
	nnoremap <F7> :TlistToggle<CR>
	map <F9>e :!p4 edit '%'<CR>

	"Window movement
	map <C-j> <C-W>j
	map <C-k> <C-W>k
	map <C-h> <C-W>h
	map <C-l> <C-W>l
	
	" Switch CWD to the directory of the open buffer
	map <leader>cd :cd %:p:h<cr>:pwd<cr>

	" Move a line of text using ALT+[jk] or Comamnd+[jk] on mac
	nmap <M-j> mz:m+<cr>`z
	nmap <M-k> mz:m-2<cr>`z
	vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
	vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`zv

	" Pressing ,ss will toggle and untoggle spell checking
	map <leader>ss :setlocal spell!<cr>

	" Shortcuts using <leader>
	map <leader>sn ]s
	map <leader>sp [s
	map <leader>sa zg
	map <leader>s? z=

	" Disable arrow keys
	nnoremap <up> <nop>
	nnoremap <down> <nop>
	nnoremap <left> <nop>
	nnoremap <right> <nop>
	inoremap <up> <nop>
	inoremap <down> <nop>
	inoremap <left> <nop>
	inoremap <right> <nop>
	nnoremap j gj
	nnoremap k gk

	" No, I don't want help
	inoremap <F1> <ESC>
	nnoremap <F1> <ESC>
	vnoremap <F1> <ESC>

	nnoremap <leader>a :Ack
" }

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
" Remember info about open buffers on close
set viminfo^=%

" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
